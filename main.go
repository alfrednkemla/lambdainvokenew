package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
)

func main() {
	input := events.APIGatewayProxyRequest{HTTPMethod: "POST"}
	var output events.APIGatewayProxyResponse
	payload, err := json.Marshal(input)
	if err != nil {
		fmt.Println("Error marshalling MyGetItemsFunction request")
		os.Exit(0)
	}
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	svc := lambda.New(sess, &aws.Config{Region: aws.String("us-east-2")})
	result, err := svc.Invoke(&lambda.InvokeInput{FunctionName: aws.String("devGetBook"), Payload: payload})
	if err != nil {
		fmt.Println("Error calling devGetBook")
		os.Exit(0)
	}
	err = json.Unmarshal(result.Payload, &output)
	if err != nil {
		fmt.Println("Error unmarshalling devGetBook response")
		os.Exit(0)
	}
	if output.StatusCode == 200 {
		fmt.Println(output.Body)
	} else {
		fmt.Println("Error bad response")
	}
}
